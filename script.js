const container = document.createElement('section');
const price = document.createElement('span');
const input = document.createElement('input');
const span1 = document.createElement('span');
const btn = document.createElement('button');
const error = document.createElement('span');

container.classList.add('bg');
span1.classList.add('sp');
error.classList.add('error');

price.innerText = 'Price ';
error.innerText = 'Please enter correct price';

input.onfocus = () => input.style.border = '3px solid green';

input.onblur = () => {
    if (+input.value == null || Number.isNaN(+input.value) || +input.value <= 0) {
        document.body.appendChild(error);
        input.style.border = '3px solid red';
        input.value = null;
    } else {
        input.style.backgroundColor = 'white';
        btn.innerText = '(X)';
        span1.innerText = `Текущая цена: ${input.value}`;

        span1.appendChild(btn);
    }
};

const removeElem = () => {
    btn.remove();
    span1.remove();
    input.value = null;
};

btn.addEventListener('click', removeElem);

document.body.appendChild(container);
container.appendChild(price);
container.appendChild(input);
container.appendChild(span1);


// document.body.prepend(container);
// container.appendChild(input);
// document.body.prepend(input);
// document.body.prepend(price);
// document.body.prepend(span1);